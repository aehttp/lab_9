/*file name: kuts_lab_9_2 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №9
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для сортування елементів рядків або стовбців числової матриці
*призначення програмного файлу: 
*варіант : №4
*/
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
void SetColor(int text ,int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background <<4)|text));	
}
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j,n,m,k,kk,tmp ,k2;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int a[n][m];
	srand(time(NULL));
	printf("Матриця a( %d, %d): \n",n,m);
		for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				a[i][j] = rand()%100-50;
				printf("%4d", a[i][j]);
			}
		cout<<endl;
		}
	cout<<endl;
	int s[n*m-1];
	int pj,pi;
	for(i=0; i<n*m-1; i++) s[i]=0;
for (k = 0; k < n; k++)  
		{
		      pi=n-1; pj=k; kk=0;
		      for (; (pi<n)&(pj<m);pi--,pj++){
		      	s[kk]=a[pi][pj];
				kk++;
		      }
				for (k2 = kk / 2; k2 > 0; k2 /= 2)
					for (i = k2; i < kk; i++){
						tmp = s[i];
						for (j = i; j >= k2; j -= k2){
							if (tmp < s[j - k2])
							s[j] = s[j - k2];
							else
							break;
							}
						s[j] = tmp;
					}
				pi=n-1; pj=k; kk=0;
		      for (; (pi<n)&(pj<m);pi--,pj++){
		      	a[pi][pj]=s[kk];
				kk++;
		      }
		}
		for (k = 1; k < n; k++)  
		{
		      pi=n-k-1; pj=0;kk=0;
		      for (; (pi<n)&(pj<m-k);pi--,pj++){
		      	s[kk]=a[pi][pj];
				kk++;
		      }
		      for (k2 = kk / 2; k2 > 0; k2 /= 2)
					for (i = k2; i < kk; i++){
						tmp = s[i];
						for (j = i; j >= k2; j -= k2){
							if (tmp < s[j - k2])
							s[j] = s[j - k2];
							else
							break;
							}
						s[j] = tmp;
					}
		      pi=n-k-1; pj=0;kk=0;
		      for (; (pi<n)&(pj<m-k);pi--,pj++){
		      	a[pi][pj]=s[kk];
				kk++;
		      }
		}
	printf("\nВідсортована матриця a( %d, %d): \n",n,m);
	for (i = 0; i < n; i++)	{
		for (j = 0; j < m; j++){
			printf("%4d", a[i][j]);
			}
		cout<<endl;
	}
	cout<<endl;
	system("pause");
	return 0;
}

