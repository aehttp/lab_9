/*file name: kuts_lab_9_1 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №9
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для сортування елементів одновимірного числового масиву 
*призначення програмного файлу: 
*варіант : №4
*/
#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <time.h>
using namespace std;
int main() {
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i,j,n,k=0, tmp;
	srand(time(NULL));
	cout<<"Введіть розмір масиву (n) = ";
	cin>>n;
	int a[n];
	printf("Матриця a( %d): \n",n);
	for (i = 0; i < n; i++){
			a[i] = rand()%100 - 50;
			if(a[i]>0 ) {
				k++;
				
			}
		printf("%4d", a[i]);
	}
	int b[k];
	j=0;
	for (i = 0; i < n; i++){
			if(a[i]>0 ) {
				b[j]=a[i];
				j++;
			}
	}
	//сортування
	for(i = 1; i < k; ++i){
		for(j = 0; j < k-i; j++){
			if(b[j] > b[j+1]){
				tmp = b[j];
				b[j] = b[j+1];
				b[j+1] = tmp;
			}
		}
	}
	j=0;
	printf("\n Відсортована Матриця a( %d): \n",n);
	for (i = 0; i < n; i++){
		if(a[i]>0) {
				a[i]=b[j];
				j++;
			}
		printf("%4d", a[i]);
	}
	cout<<endl;
	system("pause");

	return 0;
}

