/*file name: kuts_lab_9_3 
*студент: Куць Ганна Олександрівна
*група : КН-1-3
*дата створення: 15.12.2021
*дата останньої зміни 15.12.2021
*Лабораторна №9
*завдання : Розробити блок-схему алгоритму та реалізувати його мовою С\С++ для сортування елементів рядків або стовбців числової матриці
*призначення програмного файлу: 
*варіант : №4
*/
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;
int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j,n,m,k, tmp, f=0,fi;
	cout<<"Задайте кількість рядків матриці n = ";
	cin>>n;
	cout<<"Задайте кількість стовпчиків матриці m = ";
	cin>>m;
	int b[n][m];
	
	srand(time(NULL));
	printf("Матриця a( %d, %d): \n",n,m);
		for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				b[i][j] = rand()%100-50;
				printf("%4d", b[i][j]);
				if(b[i][j]<0)
					f++;
				
			}
		cout<<endl;
		}
	int a[f];
	fi=0;
	for (i = 0; i < n; i++){
			for (j = 0; j < m; j++){
				if(b[i][j]<0){
					a[fi]= b[i][j];
					fi++;
				}
			}
		}
	//сортування
	i=1;
	j=2;
	while(i<f){
		if (a[i-1]<=a[i]){
			i=j;
			j=j+1;
		}
		else{
			tmp=a[i-1];
			a[i-1]=a[i];
			a[i]=tmp;
			i--;
			if (i==0){
				i=j;
				j=j+1;
				}
		}
	}
	//запис назад
		  int r,s=-1,pi,pj;
  for (r = 0; r < (n+m-1); r++){
      if (r<m){
        pi=0;
        pj=m-r-1;
        }
      else{
        pi=r-(m-1);
        pj=0;
        }
	    for (; (pi<n)&(pj<m);pi++,pj++) {
	    	if (b[pi][pj]<0){
	    		s++;
	      		b[pi][pj]=a[s];
	    	}
    	}
    }
	
	
	printf("\n Відсортована матриця a( %d, %d): \n",n,m);
	for (i = 0; i < n; i++)	{
		for (j = 0; j < m; j++){
			printf("%4d", b[i][j]);
			}
		cout<<endl;
	}
	cout<<endl;
	system("pause");
	return 0;
}

